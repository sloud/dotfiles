#############################################
## Aliases                                 ##
#############################################

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias ll='clear; ls -lh'
alias la='clear; ls -alh'

#############################################