#############################################
## User local bin                          ##
#############################################
mkdir -p "$HOME/.local/bin"
path+=("$HOME/.local/bin")

export PATH
#############################################


#############################################
## Base zsh config                         ##
#############################################

## Load the theme. https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
antigen theme gnzh

## Bundles from the default repo. https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins-Overview
antigen bundle z
antigen bundle sudo
antigen bundle colored-man-pages

## Bundles from third-party repos
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions

#############################################