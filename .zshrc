#############################################
## Antigen                                 ##
## - https://antigen.sharats.me/           ##
## - https://github.com/zsh-users          ##
#############################################

## Install antigen if it isn't already installed
mkdir -p "$HOME/.local/lib"
if [ ! -f "$HOME/.local/lib/antigen.zsh" ]; then
    curl -L git.io/antigen >"$HOME/.local/lib/antigen.zsh"
fi

## Initialize antigen
source "$HOME/.local/lib/antigen.zsh"

## Debugging. uncomment if needed.
# export ANTIGEN_LOG="$HOME/antigen.log"

## Load the oh-my-zsh's library. https://ohmyz.sh/
antigen use oh-my-zsh

## Environment specific plugins
if [ -d "$HOME/.zshrc.d" ]; then
    for rc in $(find "$HOME/.zshrc.d" -maxdepth 1 -type f); do
        source $rc
    done
fi

## Tell Antigen that you're done.
antigen apply

#############################################
