# Dotfiles

## Installation 

### `main` branch of the config

```sh
curl -L https://sloud.gitlab.io/dotfiles/install.sh | sh
```

### `min` branch of the config

```sh
curl -L https://sloud.gitlab.io/dotfiles/install-min.sh | sh
```

## Directories

### `$HOME/.local/bin/`
Local user `bin/` folder. Executables dropped in here will be on your `$PATH`.

## Developer tools
The `.zshrc` will install a couple of dev tools locally to the user's directory. Some, not all, are:
- `nvm` - https://github.com/nvm-sh/nvm
- `pnpm` - https://pnpm.io/
- `minikube` - https://minikube.sigs.k8s.io/docs/
- `kubectl` - https://kubernetes.io/docs/reference/kubectl/kubectl/
- `flux` - https://fluxcd.io/
- `kubeseal` - https://github.com/bitnami-labs/sealed-secrets
